# OpenML dataset: CIF-2016-competition

https://www.openml.org/d/46109

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

CIF 2016 time series forecasting competition , monthly data

From the website:
-----
Competition Data Format

Data file containing time series to be predicted is a text file having the following format:

Each row contains a single time series data record;

items in the row are delimited with semicolon (";");

the first item is an ID of the time series;

the second item determines the forecasting horizon, i.e., the number of values to be forecasted;

the third item determines the frequency of the time series (this year "monthly" only);

the rest of the row contains numeric data of the time series;

the number of values in each row may differ because each time series is of different length.

Example of the competition data format:

ts1;4;yearly;26.5;38.2;5.3
ts2;12;monthly;1;2;4;5;5;6;8;9;10
...
ts72;12;daily;1;2;4;5;5;6;8;9;10

-----

There are 3 columns:

id_series: The identifier of a time series.

value: The value of the time series at 'time_step'.

time_step: The time step on the time series.


Preprocessing:

Training set

1 - Renamed first three columns to 'id_series' and 'horizon' and 'period', and renamed the other columns to reflect the actual time_step of the time series.

2 - Melted the data, obtaining columns 'time_step' and 'value'.

3 - Dropped nan values.

The nan values correspond to time series that are shorter than the time series with maximum lenght, there are no nans in the middle of a time series.

4 - Defined columns 'id_series' as 'category', casted 'time_step' to int.

Test set:

Same as for the training set. 

Finally, we have concatenated both training and test set. If one wants to use the same train and test set of the competition, we invite them to get the
forecasting horizon of the original data on the provided website.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46109) of an [OpenML dataset](https://www.openml.org/d/46109). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46109/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46109/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46109/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

